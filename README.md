wots2-q8
========
This is the default skin for the wots2 project (see <https://github.com/Biggerskimo/wots2>).

Usage
========
wots2 includes a `desktop.css`/`mobile.css` file, so you have to generate one using stylus:

    > npm install stylus
    > node_modules/stylus/bin/stylus -c -U main.styl

Or, using the `build` script, which downloads stylus if necessary and compiles:

    > ./build

